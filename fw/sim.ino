#include <Adafruit_NeoPixel.h>
#include "input.h"

static const uint8_t TOP_PIXEL_ID = 15;
static const uint8_t BOT_PIXEL_ID = 31;

static const int pin_s1 = 7;	// water level switch
static const int pin_ts1 = 8;	// temperature sensor
static Input water_valve_open(2);
static Input detergent_valve_open(3);
static Input heater_powered(4);
static Input drain_pump_active(5);
static Input motor_slow_left(9);
static Input motor_slow_right(10);
static Input motor_fast_left(11);
static Input motor_fast_right(12);

static bool have_detergent = false;
static const uint16_t water_level_max = 300;
static uint16_t water_level = 0;
static const uint8_t max_pixels = 10;
static const uint16_t water_temperature_max = 400;
static uint16_t water_temperature = 0;

static Adafruit_NeoPixel pixels(32, 6, NEO_GRB + NEO_KHZ800);

void setup() {
	Serial.begin(9600);
	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(pin_s1, OUTPUT);
	pinMode(pin_ts1, OUTPUT);
	water_valve_open.begin();
	detergent_valve_open.begin();
	heater_powered.begin();
	drain_pump_active.begin();
	motor_slow_left.begin();
	motor_slow_right.begin();
	motor_fast_left.begin();
	motor_fast_right.begin();
	pixels.begin();
}

static uint32_t calc_color() {
	const uint8_t temperature_intensity = water_temperature * 32 / water_temperature_max;
	uint8_t green = 0, blue = 0;

	if (have_detergent) {
		green = 96 - temperature_intensity / 2;
		blue = 32 - temperature_intensity / 2;
	} else {
		blue = 128 - temperature_intensity;
	}

	return pixels.Color(temperature_intensity, green, blue);
}

void animate_action(uint8_t position, int8_t speed, uint32_t color) {
	static uint8_t state = 0;
	const uint8_t offset = (uint8_t)(speed * state / 8) % 8;
	pixels.setPixelColor((uint8_t)(position + offset) % 32, color);
	pixels.setPixelColor((uint8_t)(position - offset - 1) % 32, color);
	++state;
}

static void wheel_loop() {
	static uint8_t head = BOT_PIXEL_ID * 8;
	uint8_t num_pixels = water_level * max_pixels / water_level_max;
	const uint32_t color = calc_color();

	pixels.clear();

	if (motor_slow_left) {
		head += 2;
	} else if (motor_slow_right) {
		head -= 2;
	} else if (motor_fast_left) {
		head += 8;
	} else if (motor_fast_right) {
		head -= 8;
	} else {
		const uint8_t gravity = (uint8_t)(BOT_PIXEL_ID - (num_pixels + 1) / 2) % 32 * 8;
		if ((uint8_t)(head - gravity) > 128) {
			++head;
		} else if (head != gravity) {
			--head;
		}
	}

	if ((motor_slow_left || motor_slow_right || motor_fast_left || motor_slow_right) && num_pixels < 1) {
		num_pixels = 1;
	}
	for (uint8_t i = 0; i < num_pixels; ++i) {
		pixels.setPixelColor((head / 8 + i) % 32, color);
	}

	if (water_valve_open) {
		animate_action(TOP_PIXEL_ID,  1, pixels.Color(0, 0, 8));
	} else if (detergent_valve_open) {
		animate_action(TOP_PIXEL_ID,  1, pixels.Color(0, 8, 0));
	} else if (drain_pump_active) {
		animate_action(BOT_PIXEL_ID, -1, pixels.Color(0, 0, 8));
	} else if (heater_powered) {
		animate_action(BOT_PIXEL_ID,  1, pixels.Color(8, 0, 0));
	}

	pixels.show();
}

void loop() {
	digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
	water_valve_open.update();
	detergent_valve_open.update();
	heater_powered.update();
	drain_pump_active.update();
	motor_slow_left.update();
	motor_slow_right.update();
	motor_fast_left.update();
	motor_fast_right.update();

	if (water_valve_open || detergent_valve_open) {
		water_temperature = 0;
		++water_level;
		if (water_level > water_level_max)
			water_level = water_level_max;
	}
	if (detergent_valve_open) {
		have_detergent = true;
	}

	if (drain_pump_active && water_level > 0) {
		--water_level;
		have_detergent = false;
	}

	if (heater_powered) {
		++water_temperature;
		if (water_temperature > water_temperature_max)
			water_temperature = water_temperature_max;
	}

	digitalWrite(pin_s1, water_level >= water_level_max);
	digitalWrite(pin_ts1, water_temperature >= water_temperature_max);

	wheel_loop();

	delay(10);
}
