#ifndef _INPUT_H_
#define _INPUT_H_

#include <Arduino.h>
#include <stdint.h>


class Input {
	public:
		constexpr Input(uint8_t pin)
			: pin(pin)
		{}

		operator bool() const {
			return state;
		}

		void begin() {
			pinMode(pin, INPUT);
		}

		void update() {
			cache = (cache << 1) | digitalRead(pin);
			if (cache == 0x00) {
				state = false;
			} else if (cache == 0xff) {
				state = true;
			}
		}

	private:
		bool state = false;
		uint8_t cache = 0;
		const uint8_t pin;
};

#endif
